function russe(a, b) {

    var x = [a];
    var y = [b];


    var i = 0;

    while (x[i] > 1) {
        x[i + 1] = Math.floor(x[i] / 2);
        y[i + 1] = y[i] + y[i];
        i++;
    }
   
    var prod = 0;
    while (i >= 0) {
        if(x[i] % 2) prod += y[i];
        i--;
    }

    return prod;
}