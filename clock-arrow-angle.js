function clockArrowAngle(h, m) {

    if (h >= 12) h = h - 12;

    var hourAngle = (h * 60 + m) * 0.5;

    var minuteAngle = m * 6;

    return Math.abs(minuteAngle - hourAngle);
}