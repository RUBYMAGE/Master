function excelNumberToColumn(number)
{
	/* The conversion is backwards (just like binary to decimal and vice versa). 
	 * You have to minus 1 to account for the fact that ‘A’ equals to 1 not 0.
	 */
	var r = "";
    while (number > 0) {
        r = String.fromCharCode(65 + (number - 1) % 26) + r;
        number = Math.floor((number - 1) / 26);
    }
    return r;
}