function intersection(arr1, arr2){

    //function sortNumber(a,b) {
    //    return a - b;
    //}
    //
    //arr1.sort(sortNumber);
    //arr2.sort(sortNumber);

    var i = 0;
    var j = 0;

    var n = arr1.length;
    var m = arr2.length;

    var intersection = [];

    while(i < n && j < m) {

        if(arr1[i] == arr2[j]) {
            intersection.push(arr1[i]);
            i++;
            j++;
        }
        else if (arr1[i] < arr2[j])
            i++;
        else
            j++;
    }
    return intersection;
}