function excelColumnToNumber(str)
{
	/* The O(n) solution checks for each character of the string. For example, ABC = A * 26 * 26 + B * 26 + C. 
	 * The left-most digit will be multiplied by 26 each iteration when a new digit is met. 
	 * The Uppercase ‘A’ has ASCII code of 65 but since the ‘A’ equals to 1 not 0, we have to add one manually to the result, that is -65 + 1 = -64.
	 */
	var r = 0;
	for (var i = 0; i < str.length; i ++) {
	    r = r * 26 + str[i].charCodeAt() - 64;
	}
	return r;
}