function insertionSort(list){
    for(var current = 1; current < list.length; current++){
        var el = list[current];
        var wall = current;
        while(wall > 0 && list[wall -1] > el){
            list[wall] = list[wall-1];
            wall--;
        }
        list[wall] = el;
    }
    return list;
}