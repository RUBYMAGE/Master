<?php

define('OF_SMALLER', 0); // index in state
define('OF_LARGER', 1); // index in state

$capacity = [5, 7]; // !important: lower should be the first

$initState = [0, 0];
$goalState = [0, 4];

search($initState, $goalState);


function capacityRestrictions( $state, $capacity ){
    if( $state[OF_SMALLER] > $capacity[OF_SMALLER] ) $state[OF_SMALLER] = $capacity[OF_SMALLER];
    if( $state[OF_LARGER] > $capacity[OF_LARGER] ) $state[OF_LARGER] = $capacity[OF_LARGER];
    if( $state[OF_SMALLER] < 0 ) $state[OF_SMALLER] = 0;
    if( $state[OF_LARGER] < 0) $state[OF_LARGER] = 0;
    return $state;
}

function genNewState( $state, $capacity ){

    $newStates = [];

    // Fill them
    if($state[OF_SMALLER] < $capacity[OF_SMALLER] && $state[OF_LARGER] != $capacity[OF_LARGER])
        array_push($newStates, [$capacity[OF_SMALLER], $state[OF_LARGER]]);
    if($state[OF_LARGER] < $capacity[OF_LARGER] && $state[OF_SMALLER] != $capacity[OF_SMALLER])
        array_push($newStates, [$state[OF_SMALLER], $capacity[OF_LARGER]]);

    // Empty them
    if($state[OF_SMALLER] > 0 && $state[OF_LARGER] != 0) array_push($newStates, [0, $state[OF_LARGER] ]);
    if($state[OF_LARGER] > 0 && $state[OF_SMALLER] != 0) array_push($newStates, [$state[OF_SMALLER], 0]);

    // from larger to smaller
    if( $state[OF_SMALLER] < $capacity[OF_SMALLER] ){

        $s = capacityRestrictions([
            ($state[OF_SMALLER] + $state[OF_LARGER]),
            ($state[OF_LARGER] - ($capacity[OF_SMALLER] - $state[OF_SMALLER]) )
        ], $capacity );

        if($s != $state) array_push($newStates, $s);
    }
    // from smaller to larger
    if( $state[OF_LARGER] < $capacity[OF_LARGER] ){

        $s = capacityRestrictions([
            ($state[OF_SMALLER] - ($capacity[OF_LARGER] - $state[OF_LARGER]) ),
            ($state[OF_SMALLER] + $state[OF_LARGER])
        ], $capacity );

        if($s != $state) array_push($newStates, $s);
    }

    return $newStates;
}

function search($state, $goal){

    $openStates = [$state];
    $visited = [];

    $newLine = 0; $j = 0;
    $separator = [0];

    for(;;) {
        if (empty($openStates))
            return 'failure';

        $current = array_shift($openStates);
        if( !in_array($current, $visited)) {

            echo '[' . $current[0] . ' ' . $current[1] . ']';

            if ($current == $goal) {
                return 'goal state was reached';
            }

            array_push($visited, $current);
            $newStates = genNewState($current, $GLOBALS['capacity']);
            $openStates = array_merge($openStates, $newStates);

            $c = count($newStates);
            $j += $c;
            array_push($separator, $c);
        }
        if ($separator[0] == 0) {
            if ($newLine != 0) echo '; ';
            array_shift($separator);
        }
        if ($newLine == 0) {
            echo "\n";
            $newLine = $j;
            $j = 0;
        }
        $newLine--;
        $separator[0]--;
    }
}