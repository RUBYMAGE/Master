
var matrix54 = [
[1,2,3,4],
[5,6,7,8],
[9,10,11,12],
[13,14,15,16],
[17,18,19,20]

];

function matrixSpiral(matrix, m, n){

	var top = 0;
	var right = n -1;
	var bottom = m -1;
	var left = 0;
	var direction = "right";
	var res = "";

	while(top<=bottom && left<=right){

		if(direction == "right"){
			for(var i = left; i <= right; i++){
				res = res + " " + matrix[top][i];
			}
			top++;
			direction = "down";
		} else if(direction == "down") {
			for(var i = top; i<= bottom; i++){
				res = res +  " " +matrix[i][right];
			}
			right--;
			direction = "left";
		} else if(direction=="left"){
			for(var i = right; i>=left; i--) {
				res = res +  " " +matrix[bottom][i];
			}
			bottom--;
			direction = "top";
		} else if(direction == "top"){
			for(var i = bottom; i>=top; i--){
				res = res +  " " +matrix[i][left];
			}
			left++;
			direction="right";
		}
	}

	console.log(res);
}