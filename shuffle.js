function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function swap(array, i, j) {
    var temp = array[i];
    array[i] = array[j];
    array[j] = temp;
    return array;
}

function shuffle(array) {
    var n = array.length;
    for (var i = 0; i < n; i++) {
        swap(array, i, rand(i, n - 1));
    }
    return array;
}