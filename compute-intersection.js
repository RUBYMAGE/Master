var intersection = function (a, b) {
        return a.filter(function (e) {
          // return b.includes(e);
          return b.indexOf(e) > -1;
        });
      };
      
var intersectAll = function(arrayOfArrays){
        return arrayOfArrays.reduce(intersection);
      };