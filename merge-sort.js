
function mergeSort(list){
    if(list.length == 1) {
        return list;
    }

    var l1 = list.slice(0, list.length /2);
    var l2 = list.slice(list.length /2);

    l1 = mergeSort(l1);
    l2 = mergeSort(l2);

    return merge(l1, l2);
}

function merge(a, b){
    var sorted = [];
    var i=0;
    var j=0;

    while( i < a.length && j < b.length ){

        if(a[i] < b[j]) {
            sorted.push(a[i]);
            i++;
        } else if(b[j] < a[i]) {
            sorted.push(b[j]);
            j++;
        } else {
            sorted.push(a[i]);
            sorted.push(b[j]);
            i++;
            j++;
        }
    }

    while(i < a.length) {
        sorted.push(a[i]);
        i++;
    }
    while(j < b.length) {
        sorted.push(b[j]);
        j++;
    }

    return sorted;
}