function computeUnion(array) {
    return Object.keys(array.reduce(function(a, e){ a[e] = true; return a; }, {}));
}