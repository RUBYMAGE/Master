
// Liniar memory space O(n) (we can do best)
function maximalSubarraySpace(list){
    
    var subProblem = [];
    
    subProblem[0] = Math.max(0, list[0]);
    for(var i = 1; i < list.length; i++){
        subProblem[i] = Math.max(0, subProblem[i-1] + list[i]);
    }

    var max = 0;
    for(i = 0; i < list.length; i++){
        max = Math.max(max, subProblem[i]);
    }
    
    return max;
}

// Constant memory space O(1); Liniar time O(n) (Best case)
function maximalSubarraySpace(list){
    
    var current_sum = 0;
    var max_sum = 0;
    for(var i = 0; i < list.length; i++){
        current_sum = Math.max(0, current_sum + list[i]);
        max_sum = Math.max(max_sum, current_sum);
    }
    
    return max_sum;
}