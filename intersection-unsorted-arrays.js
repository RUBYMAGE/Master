function intersection(a, b){

    if(a.length > b.length) {
        return intersection(b, a);
    }


    var i;

    // index
    var hash = new Map();

    for (i = 0; i < a.length; i++) {
        if (hash.has(a[i]))
            hash.set(a[i], hash.get(a[i]) + 1);
        else
            hash.set(a[i], 1);
    }


    // search intersection
    var r = [];

    for (i = 0; i < b.length; i++) {

        if (hash.has(b[i]) && hash.get(b[i]) > 1) {
            r.push(b[i]);
            hash.set(b[i], hash.get(b[i]) -1);
        } else if(hash.has(b[i])) {
            r.push(b[i]);
            hash.delete(b[i]);
        }
    }

    return r;
}